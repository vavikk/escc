Run:

```
curl https://install.meteor.com/ | sh
```

Then:

```
meteor
```
#Requirements
1. [X] As a user, I should be able to view the list of products with the product information.


2. [X] The user should be able to specify the quantity for each product they wish to put into the shopping cart.


3. [X] The user should be able to review their shopping cart to update the quantity or remove the product from the cart.


4. [ ] The user should be able to go back to view the list of products and add more to their shopping cart.


5. [ ] The user should be able to submit the order after which an order confirmation page should be displayed.


6. [ ] (Optional) The user should be able to close their tab or browser and return to the application with the last state of their shopping cart


# To do
### Setup
- [X] iron:router 
- [ ] collection2 
- [ ] autoform 
- [X] bootstrap ? or responsive grid
- [X] sass
- [X] Momentum iron router
- [ ] Database exporter


### Fornt End
- [X] Design, prototype a mockup
- [ ] Scenario/UI
- [X] Extend Bootstrap 3
- [X] Set a color pallete/scheme
- [X] Item/grid mixins
- [ ] Quantity scss
- [ ] Cart popup
- [ ] Shopping cart mixins
- [X] Chart icon
- [ ] Iron route transition momentum

### Secure
- [X] Remove autopublish, insecure




 