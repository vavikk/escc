Products = new Mongo.Collection('products');

ProductsService = {
    getProductList: function () {
        return Products.find({});
    },
    getProduct: function( productId ) {
        return Products.findOne(productId);
    },
    productsExist: function() {
        return Products.find().count() > 0;
    },
    generateProducts: function() {
        var productNames = [
            'HTC One (M9)',
            'Bberry Classic',
            'Google nexus 6',
            'Huawei ascend mate 2',
            'Samsung galaxy core',
            'Samsung galaxy note 4'            
        ];
        
        var description = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr";
        
        var images = [
            'htc-one-m9-front.jpg',
            'bberry-classic-front.jpg',
            'google-nexus-6-front.jpg',
            'huawei-ascend-mate-2-front.jpg',
            'samsung-galaxy-core-black-front.jpg',
            'samsung-galaxy-note-4-front.jpg',
        ];
     
        for (var i = 0; i < productNames.length; i++) {
            Products.insert({
                name: productNames[i], 
                description: description, 
                image: images[i], 
                price: this._randomPrice()
            });  
        }
    },
    _randomPrice: function() {
        return Math.floor(Random.fraction() * 10) * 5;
    }
};
