Template.confirmation.helpers({
    "order_info" : function() {
        //var cart = Cart.find( {sessid: currentUserId} ).fetch(),
        var shopCart = [],
            currentUserId = Session.get("sessId"),
            cartItems = CartItems.find( {sessid: currentUserId} ).fetch(),
            total_items = 0,
            total = 0;
     
        cartItems.forEach(function (cartitem) {
            var product = Products.findOne({_id: cartitem.product});
            cartitem.productname = product.name;
            cartitem.price = (Number(product.price) * cartitem.qty);
            total += cartitem.price;
            shopCart.push(cartitem);
            total_items += Number(cartitem.qty);
        }); 
        
        shopCart.total = total || 0;
        shopCart.total_items = 0;
         
        return shopCart;
    }

});