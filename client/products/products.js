Template.products.helpers({
    'productlist': function(){
        return ProductsService.getProductList();
    }
});

Template.product.events({
    'click .addtocart':function(evt,tmpl){
        var qty = tmpl.find('.prodqty').value;
        var product = this._id;
        var sessid =  Session.get("sessId");
        Meteor.call('addToCart', qty, product , sessid);
    }
});

Template.registerHelper('currency', function(num){
    return '$' + Number(num).toFixed(2);
});