var num = 200; //number of pixels before modifying styles

$(window).bind('scroll', function () {
    if ($(window).scrollTop() > num) {
        $('.checkout_bar').addClass('fixed');
    } else {
        $('.checkout_bar').removeClass('fixed');
    }
});