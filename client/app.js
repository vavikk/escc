Meteor.subscribe("all-products");
Meteor.subscribe("cart");
Meteor.subscribe("all-items-in-cart");


ApplicationController = RouteController.extend({
    layoutTemplate: 'AppLayout',

    onBeforeAction: function () {
        console.log('app before hook!');
        this.next();
    },

    action: function () {
        console.log('this should be overridden!');
    }
});

StoreController = ApplicationController.extend({
    action: function () {
      this.render('Store');
    }
});

ConfirmationController = ApplicationController.extend({
    show: function () {
        this.render('confirmation');
        this.next();
    },
});


Template.AppLayout.transition = function() { 
    return function(from, to, element) {
        return 'iron-router';
    }
}

Template.AppLayout.helpers({
    transitionOptions: function() { 
        return function(from, to, node) {
            return 'fade';
        };
    }
});