Template.cart.created = function() {
    var currentUserId = Meteor.default_connection._lastSessionId,
        sessionExists = Session.get("sessId");
        if(!sessionExists) {
            Session.setPersistent("sessId", currentUserId);
            Console.log("no session id")
        } else {
            // clear the session ?
            console.log("clear the session")
        }
};
Template.cart.helpers({
    'cartitems': function () {
        
        var shopCart = [],
            currentUserId = Session.get("sessId"),
            cartItems = CartItems.find( {sessid: currentUserId} ).fetch(),
            total_items = 0,
            total = 0;
     
        cartItems.forEach(function (cartitem) {
            var product = Products.findOne({_id: cartitem.product});
            cartitem.productname = product.name;
            cartitem.price = (Number(product.price) * cartitem.qty);
            total += cartitem.price;
            shopCart.push(cartitem);
            total_items += Number(cartitem.qty);
        }); 
        
        shopCart.total = total || 0;
        shopCart.total_items = total_items || 0;
         
        return shopCart;
    }, 
    
}); 

Template.cart.events({
    
    'click .checkout_button': function () {       
        var cart_holder = $('.checkout'),
            table = $('table',cart_holder),
            isOpen = Session.get('isCartOpen');
        
        if(!isOpen) {
            
           Session.set("isCartOpen", true);
            $('body').addClass('order_submit');
            cart_holder
                .addClass('checkout-open')
                .height(table.height() + 80 + "px")
                .width('100%'); 
           
        }  
       
    },
    
    'click .closecart': function () {    
        var cart_holder = $('.checkout'),
            table = $('table', cart_holder);
            
        $('body').removeClass('order_submit');
        
        cart_holder
            .removeClass('checkout-open')
            .height("67px")
            .width('67px');
        Session.set("isCartOpen", false);
    },

    // removes a cart item from the cart
    'click .removeci': function (evt, tmpl) {
        Meteor.call('removeCartItem', this._id, function (error, result) {
            if (error) {
                alert("Unable to remove");
            }
        });
    },
    
    'click .plus': function () {        
        var product = this._id;
        Meteor.call('plusOneItem', this._id, function (error, result) {
            if (error) {
                alert("Unable to to increase items number ");
            }
        });
    },
    
    'click .minus': function () {
        var product = this._id;
        var qty = this.qty;
        if (qty > 1) {
            Meteor.call('minusOneItem', this._id, function (error, result) {
                if (error) {
                    alert("Unable to decrease items number");
                }
            });
        }
    },
    
    'click .submit_btn': function() {
        var currentUserId = Session.get("sessId");
        Meteor.call('submitOrder', currentUserId, function (error, result) {
            if (error) {
                alert("Unable to submit");
            } else {
                Router.go('/confirmation')
            }
        });
    }
});