(function () {
    'use strict';
    describe('Collection: Products', function() {
        it('some products are available in the collection', function() {
            expect(Products.find().count()).toBeGreaterThan(0);
        });
    });
})();