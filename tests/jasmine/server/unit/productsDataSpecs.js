 (function () {
    'use strict';
    describe('ProductsService', function() {
        beforeEach(function () {
            MeteorStubs.install();
            mock(global, 'Products');
        });
    
        afterEach(function () {
            MeteorStubs.uninstall();
        });
        
        describe('getProductList', function() {
            it('should ask for products and return them', function() {
                var result = {};    
                spyOn(Products, 'find').and.returnValue(result);
                expect(ProductsService.getProductList()).toBe(result);
            });
        });

        describe('getProduct', function() {
            it('should ask for the product with given id and return it', function() {
                var productId = 1;
                var result = { _id: productId };
                spyOn(Products, 'findOne').and.returnValue(result);

                expect(ProductsService.getProduct(productId)).toBe(result);
                expect(Products.findOne.calls.argsFor(0)).toEqual([ productId ]);
            });
        });
        
        describe('productsExist', function() {
            it('should return true if porduct exist', function() {
                var cursor = {
                    count: function() {
                        return 1;
                    }
                };
                spyOn(Products, 'find').and.returnValue(cursor);
                
                expect(ProductsService.productsExist()).toBe(true);
            });

            it('sould return false when no product exist', function() {
                var cursor = {
                    count: function() {
                        return 0;
                    }
                };
                spyOn(Products, 'find').and.returnValue(cursor);
                
                expect(ProductsService.productsExist()).toBe(false);
            });
        });
    });
    
})();
