Meteor.methods({
    submitOrder: function(sessionId) {
        var cartItems = CartItems.find({sessid: sessionId}).fetch();
        if(cartItems.length > 0) {  
            Cart.insert({createdBy: sessionId, cartItems:cartItems});
        } else {
            console.log("Cannot submit the order, cart is empty!");
        } 
    }
});