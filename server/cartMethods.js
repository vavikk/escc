Meteor.methods({
    addToCart: function(qty, product, session) {
        if(qty > 0) {
            CartItems.insert({qty:parseInt(qty), product:product, sessid:session});
        } else{
            console.log('Quantity is Zero');
        }
    },
    removeCartItem:function(id){
        CartItems.remove({_id:id});
    },
    removeAll:function(){
        Products.remove({});
        CartItems.remove({});
    },
    plusOneItem: function(productId) {
        CartItems.update(productId, {$inc: {qty: 1}});
    },
    minusOneItem: function(productId) {
        CartItems.update(productId, {$inc: {qty: -1}});
    }
    
});