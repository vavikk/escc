Meteor.publish("all-products", function () {
    return Products.find(); 
});

Meteor.publish("cart", function () {
    return Cart.find(); 
});

Meteor.publish("all-items-in-cart", function () {
    return CartItems.find(); 
});

